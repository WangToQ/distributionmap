
// DistributionMapDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DistributionMap.h"
#include "DistributionMapDlg.h"
#include "afxdialogex.h"

#include <math.h>
#include <time.h>
#include "string.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAX_POINTS_NUM  100000

#define RADIUS   5
double max_x_value = 5;
double min_x_value = -5;

WORD xStepNum = 50;
WORD yStepNum = 40;

double distriArry[MAX_POINTS_NUM];
POINT distriChart[MAX_POINTS_NUM];
DWORD pointNum = 0;

WORD x_len = 600;
WORD y_len = 400;

WORD x_start = 200;
WORD y_start = 50;

WORD x_off = RADIUS * 2;
WORD y_off = 0;

void MapToChart(double* arryMap, DWORD mapNum, POINT* chartPoint);
void drawChart(CClientDC* dp, POINT* chartPoint, DWORD mapNum);
double randomExponential(double lambda);
double gaussrand();
void insertData(double* arryMap, WORD currentLen, double newData);

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CDistributionMapDlg 对话框


CDistributionMapDlg::CDistributionMapDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DISTRIBUTIONMAP_DIALOG, pParent)
	, m_point_num(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDistributionMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_POINT_NUM, m_point_num);
	DDV_MinMaxUInt(pDX, m_point_num, 1, 100000);
	DDX_Control(pDX, IDC_DISTRI_TYPE, m_dis_type);
}

BEGIN_MESSAGE_MAP(CDistributionMapDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(ID_GENERATE_DATA, &CDistributionMapDlg::OnClickedGenerateData)
ON_BN_CLICKED(ID_SAVE_DATA, &CDistributionMapDlg::OnClickedSaveData)
END_MESSAGE_MAP()


// CDistributionMapDlg 消息处理程序

BOOL CDistributionMapDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	//对话框窗体大小及其屏幕坐标
	CRect rectDlg;
	GetClientRect(rectDlg);//获得窗体的大小
	ScreenToClient(rectDlg);
	x_len = rectDlg.Width() * 0.75;
	y_len = rectDlg.Height() * 0.8;

	x_start = rectDlg.Width() * 0.2;
	y_start = rectDlg.Height() * 0.1;

	xStepNum = x_len / (RADIUS * 2.05);
	yStepNum = y_len / (RADIUS * 2.05);
	m_point_num = 100;
	m_dis_type.SetCurSel(0);
	UpdateData(FALSE);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CDistributionMapDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CDistributionMapDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
	CClientDC drawpaint(this);
	drawChart(&drawpaint, distriChart, pointNum);
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CDistributionMapDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void MapToChart(double* arryMap, DWORD mapNum, POINT* chartPoint)
{
	WORD i = 0;
	
	double mapStep = (max_x_value - min_x_value) / xStepNum;
	//double mapStep = (arryMap[mapNum - 1] - arryMap[0]) * 1.2 / xStepNum;
	float xStep = x_len / xStepNum;
	float yStep = y_len / yStepNum;

	double currentMap = min_x_value + mapStep;
	float currentX = x_start + x_off;
	float currentY = y_start + y_len - yStep;

	while (i < mapNum)
	{

		if (arryMap[i] < currentMap)
		{
			chartPoint[i].x = currentX;
			
			if (currentY < y_start + RADIUS )
			{
				chartPoint[i].y = chartPoint[i - 1].y;
			}
			else
			{
				chartPoint[i].y = currentY;
			}
			currentY -= yStep;
			i++;
		}
		else
		{
			currentY = y_start + y_len - yStep;
			currentMap += mapStep;
			currentX += xStep;
		}
	}
}

void drawChart(CClientDC* dp, POINT* chartPoint, DWORD mapNum)
{
	CBrush brush, *oldbrush;
	brush.CreateSolidBrush(RGB(0, 0, 0));
	oldbrush = dp->SelectObject(&brush);

	CBrush brush_rec(RGB(5, 255, 255));
	POINT point[2] = { { x_start, y_start },{ x_start + x_off + x_len, y_len + y_start + y_off } };
	dp->FillRect(CRect(point[0], point[1]), &brush_rec);
	if (!mapNum)
	{
		return;
	}

	CString strCood;
	CRect	textRect(x_start - 35, y_start + y_len + y_off, x_start + 35, y_start + y_len + y_off + 20);

    strCood.Format("%.3f", min_x_value);
    dp->SetTextColor(RGB(0, 0, 0));
	dp->DrawText(strCood, &textRect, DT_CENTER);

	textRect.SetRect(x_start + x_off + x_len - 35, y_start + y_len + y_off,
		             x_start + x_off + x_len + 35, y_start + y_len + y_off + 20);
	strCood.Format("%.3f", max_x_value);
	dp->DrawText(strCood, &textRect, DT_CENTER);

	point[0].x = x_start;
	point[0].y = y_len + y_start;
	point[1].x = x_start + x_len + x_off;
	point[1].y = y_len + y_start;
	dp->MoveTo(point[0]);
	dp->LineTo(point[1]);

	point[0].x = x_start + x_len / 2;
	point[0].y = y_start;
	point[1].x = x_start + x_len / 2;
	point[1].y = y_len + y_start;
	dp->MoveTo(point[0]);
	dp->LineTo(point[1]);

	for (WORD i = 0; i < mapNum; i++)
	{
		dp->Ellipse(chartPoint[i].x - RADIUS, chartPoint[i].y - RADIUS,
			        chartPoint[i].x + RADIUS, chartPoint[i].y + RADIUS);

	}
	dp->SelectObject(oldbrush);

}
double randomExponential(double lambda)
{
	double pV = 0.0;
	while (true)
	{
		pV = (double)rand() / (double)RAND_MAX;
		if (pV != 1)
		{
			break;
		}
	}
	pV = (-1.0 / lambda)*log(1 - pV);
	return pV;
}

double gaussrand()
{
	static double V1, V2, S;
	static int phase = 0;
	double X;

	double U1, U2;

	if (phase == 0) {
		do {
			U1 = (double)rand() / RAND_MAX;
			U2 = (double)rand() / RAND_MAX;
			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
		} while (S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	}
	else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;

	return X;
}

void insertData(double* arryMap, WORD currentLen,  double newData)
{
	WORD j,i;

	if (!currentLen)
	{
		arryMap[0] = newData;
	}
	else
	{
		for ( j = 0; j < currentLen; j++)
		{
			if (arryMap[j] >= newData)
			{
				break;
			}
		}
		for (i = currentLen; i > j; i--)
		{
			arryMap[i] = arryMap[i - 1];
		}
		arryMap[j] = newData;
	}
}

void CDistributionMapDlg::OnClickedGenerateData()
{
	// TODO: 在此添加控件通知处理程序代码
	CClientDC drawpaint(this);

	pointNum = GetDlgItemInt(IDC_POINT_NUM);
	m_point_num = pointNum;
	if (pointNum > MAX_POINTS_NUM)
	{
		CString warnStr;
		warnStr.Format("点数过多，请输入小于 %d 的数!", MAX_POINTS_NUM);
		AfxMessageBox(warnStr);
		return;
	}
	srand((unsigned)time(NULL));

	if (m_dis_type.GetCurSel() == 0)
	{
		for (DWORD i = 0; i < pointNum; i++)
		{
			insertData(distriArry, i, gaussrand());
		}

		if (fabs(distriArry[0]) > fabs(distriArry[pointNum - 1]))
		{
			max_x_value = fabs(distriArry[0]) * 1.2;
		}
		else
		{
			max_x_value = fabs(distriArry[pointNum - 1]) * 1.2;
		}

		min_x_value = -max_x_value;

	}
	else if (m_dis_type.GetCurSel() == 1)
	{
		for (DWORD i = 0; i < pointNum; i++)
		{
			insertData(distriArry, i, randomExponential(10));
		}

		min_x_value = 0;
		max_x_value = distriArry[pointNum - 1] * 1.1;
	}


	MapToChart(distriArry, pointNum, distriChart);

	drawChart(&drawpaint, distriChart, pointNum);

}

void CDistributionMapDlg::OnClickedSaveData()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!pointNum)
	{
		MessageBox("请生成随机数后重试！");
		return;
	}

	CString fileFullName;
	static char szFilter[] = "文本文件(*.txt)|*.txt| All Files (*.*) |*.*||";
	CFileDialog dlg(FALSE, ".txt", NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter);

	if (dlg.DoModal() == IDOK)
	{
		fileFullName = dlg.GetPathName();
		FILE *fpWrite = fopen(fileFullName, "w");

		if (fpWrite == NULL)
		{
			MessageBox("导出失败！");
			return;
		}

		for (DWORD i = 0; i < pointNum;)
		{
			fprintf(fpWrite, "%f  ", distriArry[i]);
			i++;
			if (i % 10 == 0)
			{
				fprintf(fpWrite, "\r\n");
			}
		}
		fclose(fpWrite);
		MessageBox("导出成功！\n文件路径：" + fileFullName);

	}

}
