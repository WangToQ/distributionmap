
// DistributionMapDlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// CDistributionMapDlg 对话框
class CDistributionMapDlg : public CDialogEx
{
// 构造
public:
	CDistributionMapDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DISTRIBUTIONMAP_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedGauss();
	afx_msg void OnBnClickedExpon();
	// 生成随机数的个数
	DWORD m_point_num;
	// 选择生成随机数的类型
	CComboBox m_dis_type;
	afx_msg void OnClickedGenerateData();
	afx_msg void OnClickedSaveData();
};
